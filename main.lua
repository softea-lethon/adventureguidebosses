-- Addon
AdventureGuideBosses = CreateFrame("Frame", "AdventureGuideBosses", UIParent)
local Addon = AdventureGuideBosses

-- Constants
local WING_LABEL_OFFSET = 20

-- Register for game events
Addon:RegisterEvent("PLAYER_ENTERING_WORLD")

-- Data here can be determined by examining the data here, without expansion
-- https://wowpedia.fandom.com/wiki/UiMapID      -- For map ids
-- https://wowpedia.fandom.com/wiki/LfgDungeonID -- For wing labels
-- Put the name of the raid into wowhead and lookup the matching achievement
-- Order of the bosses in the achievement is the order of the bosses in wing
-- (If it has an achievement, not all do, ie, Castle Nathria does not)
local EJOrder = {
-- UiMapID   Boss Button Order                   Raid Wing Label Index, Key is Boss Index, Value is dungeonID
    [409]  = {{1,2,3,4,5,6,7,8},                  {[1]=416,[5]=417}},                          -- Dragon Soul
    [471]  = {{1,2,3,4,5,6},                      {[1]=527,[4]=528}},                          -- Mogu'shan Vaults
    [474]  = {{1,2,3,4,5,6},                      {[1]=529,[4]=530}},                          -- Heart of Fear
    [508]  = {{1,2,3,4,5,6,7,8,9,10,11,12},       {[1]=610,[4]=611,[7]=612,[10]=613}},         -- Throne of Thunder
    [557]  = {{1,2,3,4,5,6,7,8,9,10,11,12,13,14}, {[1]=716,[5]=717,[9]=724,[12]=725}},         -- Siege of Orgrimmar
    [612]  = {{1,2,4,3,5,6,7},                    {[1]=849,[3]=850,[7]=851}},                  -- Highmaul
    [597]  = {{4,1,7,2,5,8,3,6,9,10},             {[4]=847,[2]=846,[3]=848,[10]=823}},         -- Blackrock Foundry
    [661]  = {{1,2,3,5,4,6,7,8,11,9,10,12,13},    {[1]=982,[5]=983,[7]=984,[9]=985,[13]=986}}, -- Hellfire Citadel
    [777]  = {{1,2,3,4,5,6,7},                    {[1]=1287,[4]=1288,[7]=1289}},               -- The Emerald Nightmare
    [764]  = {{1,2,3,4,7,8,5,6,9,10},             {[1]=1290,[4]=1291,[5]=1292,[10]=1293}},     -- The Nighthold
    [850]  = {{1,3,5,2,4,6,7,8,9},                {[1]=1494,[2]=1495,[7]=1496,[9]=1497}},      -- Tomb of Sargeras
    [909]  = {{1,2,3,4,5,6,7,8,9,10,11},          {[1]=1610,[4]=1611,[7]=1612,[10]=1613}},     -- Antorus, the Burning Throne
    [1148] = {{1,2,4,3,5,6,7,8},                  {[1]=1731,[3]=1732,[7]=1733}},               -- Uldir
    [1352] = {{1,3,2,4,5,6,7,8},                  {[1]=1945,[4]=1946,[7]=1947}},               -- Battle of Dazar'alor (Alliance)
    [1358] = {{1,2,3,4,5,6,7,8},                  {[1]=1948,[4]=1949,[7]=1950}},               -- Battle of Dazar'alor (Horde)
    [1512] = {{1,2,3,4,5,6,7,8,9,10},             {[1]=2009,[4]=2010,[7]=2011}},               -- The Eternal Palace
    [1581] = {{1,2,3,4,9,5,10,6,7,8,11,12},       {[1]=2036,[4]=2037,[6]=2038,[11]=2039}},     -- Ny'alotha
    [1735] = {{2,5,6,3,4,7,1,8,9,10},             {[2]=2090,[3]=2091,[1]=2092,[10]=2096}},     -- Castle Nathria
	  [1998] = {{1,2,3,4,5,6,7,8,9,10},             {[1]=2221,[4]=2222,[7]=2223,[10]=2224}},     -- Sanctum of Domination
	  [2047] = {{4,5,6,1,2,3,7,8,9,10,11},          {[4]=2291,[1]=2292,[8]=2293,[11]=2294}},     -- Sepulcher of the First Ones
}

-- Hook EJ_GetEncounterInfoByIndex and return the LFR wing order of bosses.
local Blizzard_EJ_GetEncounterInfoByIndex = EJ_GetEncounterInfoByIndex
EJ_GetEncounterInfoByIndex =
function(...)
  local index, journalInstanceID = ...
  local uiMapID = select(7, EJ_GetInstanceInfo())
  if EJOrder[uiMapID] then
    local order = EJOrder[uiMapID][1]
    index = order[index] or index
  end
  return Blizzard_EJ_GetEncounterInfoByIndex(index, journalInstanceID)
end

--
-- Processes WoW events that this addon has registered to react to.
-- @param event The event.
-- @param ...   The event parameters.
--
function Addon:OnEvent(event)
  if event == "PLAYER_ENTERING_WORLD" then
    if not EncounterJournal then
      -- This bypasses the new lazy loading of Blizzards new UI system.
      -- However, its the easiest way to get all the event/function hooking going.
      EncounterJournal_LoadUI()
      -- The instance chosen here doesn't matter, as long as its valid to get the GUI elements populated
      -- I chose Uldir because its what I used in the screen shot for curseforge
      -- I have no idea why I originally chose that one
      -- This also uses a 'journalInstanceID' which has no authoritative list as far as I can tell,
      -- so this might break patch to patch?
      EncounterJournal_DisplayInstance(1031, true)
      -- Reset to encounter journal home page
      EJSuggestFrame_OpenFrame()

      local scrollbox = EncounterJournalEncounterFrameInfo.BossesScrollBox
      scrollbox.Blizzard_GetDerivedExtent = scrollbox.GetDerivedExtent
      scrollbox.GetDerivedExtent =
      function(...)
        -- Inflate the size of the scrollable area to account for wing labels
        local extent = scrollbox.Blizzard_GetDerivedExtent(...)
        local UiMapID = select(7, EJ_GetInstanceInfo())
        if EJOrder[UiMapID] then
          extent = extent + 3 * WING_LABEL_OFFSET
        end
        return extent
      end
    end

    -- Create a callback for when a boss button frame is created/pull from the frame pool
    local AcquiredFrame =
    function(...)
      local _, frame, elementData = ...
      -- Add a wing label to the button, and functions to report to the
      -- layout system that our button is bigger than it actually is
      local UiMapID = select(7, EJ_GetInstanceInfo())
      if EJOrder[UiMapID] and elementData then
        -- Buttons are pulled from a frame pool, the top to bottom index can be identified with GetOrderIndex
        local bossIndex = frame:GetOrderIndex()
        local order = EJOrder[UiMapID][1]
        local rfinfo = EJOrder[UiMapID][2][order[bossIndex]]

        if not rfinfo then
          -- This boss isn't the start of a wing, its being reused from pool
          if frame.wingFrame then frame.wingFrame:Hide() end
          if frame.Blizzard_GetHeight then frame.GetHeight = frame.Blizzard_GetHeight end
          if frame.Blizzard_SetPoint then hooksecurefunc(frame, "SetPoint", frame.Blizzard_SetPoint) end
          return
        end

        if not frame.wingFrame then
          frame.wingFrame = CreateFrame("Frame", "LFDWingFrame", frame)
          frame.wingLabel = frame.wingFrame:CreateFontString( "LFDWingLabel", "ARTWORK","QuestTitleFont")
        end

        local label = GetLFGDungeonInfo(rfinfo)
        frame.wingLabel:SetText(label)
        frame.wingFrame:SetWidth(frame.wingLabel:GetStringWidth(label))
        frame.wingFrame:SetHeight(frame.wingLabel:GetStringHeight(label))
        frame.wingLabel:SetAllPoints()
        frame.wingFrame:Show()
        frame.wingFrame:SetPoint("CENTER", frame, "CENTER", 10, 40)

        -- This was hooked elsewhere, unhook it, then rehook it
        -- Could also have called around the hook by using
        -- frame.Blizzard_GetHeight = getmetatable(frame).__index.GetHeight
        frame.GetHeight = nil
        frame.Blizzard_GetHeight = frame.GetHeight
        frame.GetHeight =
        function(...)
          -- Inflate the reported size of the button with the space for a label
          -- This is what allows the rest of the buttons to layout correctly
          return frame.Blizzard_GetHeight(...) + WING_LABEL_OFFSET
        end

        frame.SetPoint = nil
        frame.Blizzard_SetPoint = frame.SetPoint
        frame.SetPoint =
        function(...)
          local _, point, relativeTo, relativePoint, offsetX, offsetY = ...
          -- Move the button below the label, but still inside the space provided for by the layout system
          offsetY = offsetY - WING_LABEL_OFFSET
          frame.Blizzard_SetPoint(frame, point, relativeTo, relativePoint, offsetX, offsetY)
        end
      end
    end

    -- Create a callback for when a boss button frame is released back to the frame pool
    local ReleasedFrame =
    function(...)
      -- Remove label data and restore normal button functionality
      local _, frame = ...
      if frame.wingFrame then frame.wingFrame:Hide() end
      if frame.Blizzard_GetHeight then frame.GetHeight = frame.Blizzard_GetHeight end
      if frame.Blizzard_SetPoint then hooksecurefunc(frame, "SetPoint", frame.Blizzard_SetPoint) end
    end

    -- Add callbacks for when buttons are acquired and released by the scroll system frame pool
    ScrollUtil.AddAcquiredFrameCallback(EncounterJournalEncounterFrameInfo.BossesScrollBox, AcquiredFrame, nil, false)
    ScrollUtil.AddReleasedFrameCallback(EncounterJournalEncounterFrameInfo.BossesScrollBox, ReleasedFrame, nil)
  end
end

Addon:SetScript("OnEvent", Addon.OnEvent)
